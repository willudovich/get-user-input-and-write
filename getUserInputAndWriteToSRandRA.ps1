#User Input to Description Script
#v.02

#Declare Variables
$activityArray = @()
$userInputArray = @()
$wiObject = $null
$currentDescription = ""
$updatedDescriptionArray = @()
$updatedObject = $null

#load modules
import-module smlets

#sr with lots of input
$srGUID = "e9b5a8c3-8e4c-4eb9-13f0-371cf98570db"
$srObject = get-scsmobject -ID $srGUID


Function fn-get-user-input
{
    Param (
            $wiObject
            )

    #Declare Vars
    $userInput = ""
    $Array = @()
    $ListArray = @()

    #### Modified, but originally from Blog
    #### http://itblog.no/4462
    $UserInput = $wiObject.UserInput
    $nl = [Environment]::NewLine
    $content=[XML]$UserInput
    $inputs = $content.UserInputs.UserInput
    foreach ($input in $inputs)
    {
        if($($input.Answer) -like "<value*")
        {       
            [xml]$answer = $input.answer
            foreach($value in $($($answer.values)))
            {
                    foreach($item in $value)
                    {                   
                        foreach ($txt in $($item.value))
                        {                     
                            $ListArray += $($txt.DisplayName)                  
                        }
                        $Array += $input.Question + " = " + [string]::Join(", ",$ListArray)
                        $ListArray = $null
                    }
            }
        }
        else 
        {
             if ($input.type -eq "enum")
            {
                $ListGuid = Get-SCSMEnumeration -Id $input.Answer
                $Array += $($input.Question + " = " + $ListGuid.displayname)
            }
            else 
            {
            $Array += $($input.Question + " = " + $input.Answer)
            }
        }
    }
    $Array
}

Function fn-get-current-description
{
    param(
        $wiObject
        )

        $wiFieldValues = $wiObject.values
        $wiDescription = $wiFieldValues | ?{$_.type -match "Description"} | select value 
        $wiDescriptionString = $wiDescription.value

        $wiDescriptionString

}

Function fn-create-new-decription
{
    param(
        $userInput,
        $currentDescription
        )

        #Declare Vars
        $DescriptionArray = @()

        $DescriptionArray += "************ Begin User Input Information **********"
        $DescriptionArray += ""
        $DescriptionArray += $userInput
        $DescriptionArray += ""
        $DescriptionArray += "************ End User Input Information **********"
        $DescriptionArray += ""
        $DescriptionArray += "************ Begin Original Description **********"
        $DescriptionArray += ""
        $DescriptionArray += $currentDescription

        $DescriptionArray

}

Function fn-update-object
{
    param(
        $wiObject,
        $updatedDescriptionArray,
        $currentDescription
        )

       $updatedDescription = [string]::Join("`r`n",$updatedDescriptionArray)


        if ($currentDescription -notmatch "Begin User Input Information")
        {
            $wiObject | Set-SCSMObject -Property "Description" -Value $updatedDescription
        }

}

Function fn-get-related-review-activities
{
    param(
        $srObject
        )
    
    #get related activity relationship
    $relatedActivityRelationshipObject = Get-SCSMRelationshipClass -ID "2da498be-0485-b2b2-d520-6ebd1698e61b"

    #get related activities by relationship
    $relatedActivities = Get-SCSMRelatedObject -SMObject $srObject -Relationship $relatedActivityRelationshipObject

    #return only review activites
    foreach ($activity in $relatedActivities)
    {
        if ( $activity.classname -eq "System.WorkItem.Activity.ReviewActivity" )
        {
            $activityArray += $activity
            break
        }
        elseif ( $activity.classname -eq "System.WorkItem.Activity.SequentialActivity" -or "System.WorkItem.Activity.ParallelActivity")
        {
            fn-get-related-review-activities -srObject $activity
        }
    }
    $activityArray
}




#### Main ####

$userInputArray = fn-get-user-input -wiObject $wiObject

if ($userInputArray -ne $null)
{

    $currentDescription = fn-get-current-description -wiObject $srObject
    $updatedDescriptionArray = fn-create-new-decription -userInput $userInputArray -currentDescription $currentDescription
    $updatedObject = fn-update-object -wiObject $srObject -currentDescription $currentDescription -updatedDescriptionArray $updatedDescriptionArray

    $raObjects = fn-get-related-review-activities -srObject $srObject

    $currentDescription = fn-get-current-description -wiObject $raObjects
    $updatedDescriptionArray = fn-create-new-decription -userInput $userInputArray -currentDescription $currentDescription
    $updatedObject = fn-update-object -wiObject $raObjects -currentDescription $currentDescription -updatedDescriptionArray $updatedDescriptionArray

}