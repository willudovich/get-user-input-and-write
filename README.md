SCSM_get-user-input-and-write
===================

Takes User Input from SCSM Work Items and writes to to other items

**Summary:**

- This script takes in a single SCSM Object GUID!
- It is intended to be triggered from Orchestrator. Specifically, a 'Monitor' object, looking for 'New, Service Requests.'
- Passing the SC Object GUID to this script, and replacing the test GUID on line 15 with the Orchestrator Published Value will make this all work.
- It will write the User Input in the parent Work Item (An Incident or a Service Request) to any Review Activities related to the parent object. If you don't want it to do that, comment out lines 167 to 171.

**Changelog:**

- Version .02
	- Was missing an array declaration so when selecting multiple objects in a picker, they would not be properly split.
	- Changed splitter to ", " rather than " ; "
	- NOTE: Review Activity Nesting Code is Known to be Not Working! 
- Version .01
	- Initial Version
	- Minimal error checking, but it works!